﻿using UnityEngine;
//using System.Collections;

public static class VectorExtensions{


	public static Vector3 xy0(this Vector2 v){
		
		return (Vector3)v;
	}

	public static Vector3 xy0(this Vector3 v){
		v.z = 0;
		return v;
	}

	public static Vector3 xy0(this Vector4 v){

		return (Vector3)v;
	}



	public static Vector4 xy00(this Vector2 v){
		return (Vector4)v;
	}
	
	public static Vector4 xyz0(this Vector3 v){
		return (Vector4)v;
	}


}
