﻿using UnityEngine;
using System.Collections;

public static class Utils {

	public static float Clampf(float f, float v){
		return f < -v ? -v : f > v ? v : f;
	}

	public static float Clampf(float f, float min, float max){
		return f < min ? min : f > max ? max : f;
	}


}
