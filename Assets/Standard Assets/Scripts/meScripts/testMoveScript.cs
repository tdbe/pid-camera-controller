﻿using UnityEngine;
using System.Collections;

public class testMoveScript : MonoBehaviour {

	public float force = 10.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Input.GetKey(KeyCode.Keypad4)){

			rigidbody.AddForce(new Vector3(-force, 0, 0));
			//Debug.Log(rigidbody.velocity);
		}
		if(Input.GetKey(KeyCode.Keypad6)){
			
			rigidbody.AddForce(new Vector3(force, 0, 0));
			//Debug.Log(rigidbody.velocity);
		}
		if(Input.GetKey(KeyCode.Keypad8)){
			
			rigidbody.AddForce(new Vector3(0, 0, force));
			//Debug.Log(rigidbody.velocity);
		}
		if(Input.GetKey(KeyCode.Keypad5)){
			
			rigidbody.AddForce(new Vector3(0, 0, -force));
			//Debug.Log(rigidbody.velocity);
		}
	}
}
