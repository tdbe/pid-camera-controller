﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	[SerializeField]
	public PIDProcessor cameraPositionPIDProcessor;

	public Camera targetCamera;
	public Transform targetCameraRig;
	public Transform targetCameraPivot;
	public Transform targetOfCamera;
	public Transform targetOfTargetOfCamera;//maybe the player has locked on to an enemy

	[System.Serializable]
	public class Settings
	{
		public bool hideMouseCursor = false;

		[Range(0f,10f)]
		public float turnSpeed = 1.5f;    //how fast the rig will rotate from user input.
		[Range(0f,1f)]
		public float smoothingRotationInput = 0.0f;
		public float tiltMax = 75f;       //the maximum value of the x axis rotation of the pivot.
		public float tiltMin = 45f;       //the minimum value of the x axis rotation of the pivot.
	}
	[SerializeField]
	private Settings settings;//initialized by unity, because serializable 

	private float rigRotation;
	private float pivotRotation;
	private float smoothX = 0;
	private float smoothY = 0;
	private float smoothXvelocity = 0;
	private float smoothYvelocity = 0;

	// Use this for initialization
	void Start () {

		//http://blogs.unity3d.com/2012/10/25/unity-serialization/
		//http://forum.unity3d.com/threads/serialization-best-practices-megapost.155352/
		//TODO Serialization best practices.
		cameraPositionPIDProcessor.initializeProcessor(targetCameraRig.position, targetOfCamera.position);
	}


	//TODO: need a var for camera distance /scrolling 

	/// <summary>
	/// Called from the target's controller script (if target has such a thing)
	/// the moment the target becomes airbourne.
	/// </summary>
	public void onTargetAirbourne(){
		//TODO: camera zooms out a bit, and pivot centers itself on player.
	}

	/// <summary>
	/// Called from the target's controller script (if target has such a thing)
	/// the moment the target becomes grounded.
	/// </summary>
	public void onTargetGrounded(){
		//TODO: Note: the camera controller by default assumes the character is grounded if none of these functions get called.

		//TODO: -when you land, camera first zooms in, and then moves to the right of the player.
	}


	void Update(){

		Screen.lockCursor = settings.hideMouseCursor;

		//calculate input (mouse / analog / tilt) rotation
		solveRotationFromInput();
	}

	void LateUpdate(){
		
	}

	void FixedUpdate(){

		//get input position (position of the camera's target, the player character)

		//update camera position based on player position.
		//Must be done in fixed update if the target has a rigidbody (thus is updated with physics)
		//TODO: implement a delegate to choose b/w FixedUpdate and LateUpdate, so camera can also
		//work on objects that are updated in Update.
		targetCameraRig.position = cameraPositionPIDProcessor.doUpdate(targetOfCamera.position, targetCameraRig.position);


		//if enabled, update auto rotation based on input <60* && >-60*
		//-when you rotate, if angle is <60 && >-60, then you turn camera to the left/right, 
		//and character runs in a circle. so somehow an an angle between strafing + camera angle

		//update camera rotation from mouse input / manual camera axis 
		//TODO: if no manual rotation happened for x seconds, enable using PID to reset rotation with rotation/quaternions: http://answers.unity3d.com/questions/197225/pid-controller-simulation.html
		//TODO: remember to reset pid error history when pid finishes the rotation resetting OR WHEN the pid is interrupted BY A MANUAL INPUT (so when PID is sent an INTERRUPT argument (you can pause it, but you can also interrupt/reset it))
	}

	void OnDrawGizmos()
	{
		if (targetCameraRig != null && targetCameraPivot != null && targetCamera != null)
		{
			Gizmos.color = new Color(1,1,1,1f);
			Gizmos.DrawLine(targetCameraRig.position, targetCameraPivot.position);
			//Gizmos.color = new Color(1,1,1,1f);
			Gizmos.DrawLine(targetCameraPivot.position, targetCamera.transform.position);
		}
	}

	private void solveRotationFromInput(){

		//this is an input utility from unity's new standard assets.
		#if CROSS_PLATFORM_INPUT
		float x = CrossPlatformInput.GetAxis("Mouse X");
		float y = CrossPlatformInput.GetAxis("Mouse Y");
		#else
		float x = Input.GetAxis ("Mouse X");
		float y = Input.GetAxis ("Mouse Y");
		#endif
		
		//smooth the user input
		if (settings.smoothingRotationInput > 0)
		{
			smoothX = Mathf.SmoothDamp(smoothX, x, ref smoothXvelocity, settings.smoothingRotationInput);
			smoothY = Mathf.SmoothDamp(smoothY, y, ref smoothYvelocity, settings.smoothingRotationInput);
		} else {
			smoothX = x;
			smoothY = y;
		}
		
		//adjust the look angle by an amount proportional to the turn speed and horizontal input.
		rigRotation += smoothX * settings.turnSpeed;
		
		//the cameraRig rotates around the Y axis, and the pivot rotates around the x axis.
		targetCameraRig.rotation = Quaternion.Euler (0f, rigRotation, 0f);

		//from standard assets; untested
		#if MOBILE_INPUT 
		//For tilt input, we need to behave differently depending on whether we're using mouse or touch input:
		//on mobile, vertical input is directly mapped to tilt value, so it springs back automatically when the look input is released
		//we have to test whether above or below zero because we want to auto-return to zero even if min and max are not symmetrical.
		if(y>0) tiltAngle = Mathf.Lerp(0,-settings.tiltMin, smoothY);
		if(y<=0) tiltAngle = Mathf.Lerp (0,settings.tiltMax, -smoothY);
		#else
		// on platforms with a mouse, we adjust the current angle based on Y mouse input and turn speed
		pivotRotation -= smoothY * settings.turnSpeed;
		// and make sure the new value is within the tilt range
		pivotRotation = Mathf.Clamp(pivotRotation, -settings.tiltMin, settings.tiltMax);
		#endif
		//TODO: test with controller

		targetCameraPivot.localRotation = Quaternion.Euler(pivotRotation, 0f, 0f);
	}
}
