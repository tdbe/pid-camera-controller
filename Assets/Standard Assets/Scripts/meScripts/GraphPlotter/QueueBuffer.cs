﻿using System;
using System.Collections;
using System.Collections.Generic;

public class QueueBuffer<T>{
	

	public int Count{get{return count;}}
	private int count;

	private Queue<T> queue;

	public QueueBuffer(int n){
		count = n;
		queue = new Queue<T>(n);
	}

	public void addItem(T item){
		if(queue.Count == count){

			queue.Dequeue();
		}

		queue.Enqueue(item);
	}

	public T[] GetArray(){
		return queue.ToArray();
	}

	public Queue<T> getQueue(){
		return queue;
	}
	
}