﻿//using UnityEngine;
using System.Collections;

public class StandardPIDGraphSet{



	public readonly int Slot;
	//note to self: https://stackoverflow.com/questions/755685/c-static-readonly-vs-const
	public readonly int T = 0;
	public readonly int P = 1;
	public readonly int I = 2;
	public readonly int D = 3;
	
	private const int nrOfGraphs = 4;
	private GraphManager graphMan;

	private bool active = true;

	/// <summary>
	/// A set of 4 graphs, one for the Target value and the rest for the P, I and D components.
	/// This class stores intexes, for readability and convenience.
	/// </summary>
	public StandardPIDGraphSet(GraphManager graphMan){

		this.graphMan = graphMan;
		Slot = graphMan.createSlotWithGraphs(StandardPIDGraphSet.nrOfGraphs);
		active = true;
	}

	public void removeThisGraphSlot(){
		if(active){
			graphMan.removeThisSlot(Slot);
			active = false;
		}
	}

	public void updatePIDGraphs(float outp, float P, float I, float D){
		if(active){
			graphMan.pushPointToGraph(Slot, T, outp);
			
			graphMan.pushPointToGraph(Slot, this.P, P);
			graphMan.pushPointToGraph(Slot, this.I, I);
			graphMan.pushPointToGraph(Slot, this.D, D);
			graphMan.pushPointToGraph(Slot, this.D+1, 0);
		}
	}
}
