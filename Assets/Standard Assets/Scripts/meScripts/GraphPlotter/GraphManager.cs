﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GraphManager : MonoBehaviour {

	public bool renderGraphs = true;

	public GameObject background;

	public List<Transform> anchors;
	public List<Material> materials;


	[SerializeField]
	private int standardGraphLength = 100;
	[SerializeField]
	private float graphScale = 0.001f;
	private List<Slot> graphSlots;
	
	private class Slot{

		//private List<GraphDrawer2D> Graphs{ get{return graphs;}}
		public List<GraphDrawer2D> graphs;

		public int GraphLength{ get{ return graphLength;}}
		public Transform Anchor { get{ return anchor;}}

		private int graphLength;
		private Transform anchor;

		public Slot(Transform anchor, int graphLength){
			graphs = new List<GraphDrawer2D>();
			this.graphLength = graphLength;
			this.anchor = anchor;
		}

		public void insertGraph(List<Material> mats){
			GraphDrawer2D g = new GraphDrawer2D(graphLength, 
			                                    mats.Count >= graphs.Count ? 
			                                    mats[graphs.Count] : 
			                                    mats[0]);
			graphs.Add(g);
		}

		public void renderGraphList(float graphScale){
			for(int i=0; i<graphs.Count; i++){
				graphs[i].doDrawPass(graphScale);
			}
		}

	}

	void Awake() {
		graphSlots = new List<Slot>();
	}
	

	void OnPostRender() {
		if(renderGraphs){
			for(int i=0; i< graphSlots.Count; i++){

				graphSlots[i].renderGraphList(graphScale);
			}
		}
	}

	//TODO: this class needs to be capable of inserting a new slot into a grid on screen, 
	//autoamtically picking a free slot if there are already slots in use
	public int createSlotWithGraphs(int nrOfGraphs){
		//if(!background.activeSelf){
			//note: this is a quick dumb solution. will fix when the todo above gets implemented
		//	background.SetActive(true);
		//}

		Slot s = new Slot(//anchors.Count >= graphSlots.Count ?
		                  //anchors[graphSlots.Count] : 
		                  anchors[0],
		                  standardGraphLength);
		graphSlots.Add(s);

		for(int i=0; i<= nrOfGraphs; i++){
		
			s.insertGraph(materials);
		}
		return graphSlots.Count-1;
	}

	public void removeThisSlot(int slot){
		graphSlots.RemoveAt(slot);
	}

	public void pushPointToGraph(int slot, int graph, float p){

		Vector2 point;// = Vector2.zero;
		point.x = graphSlots[slot].Anchor.localPosition.x;
		point.y = graphSlots[slot].Anchor.localPosition.y + p;
		graphSlots[slot].graphs[graph].loadNode(point);
	}

	public void pushPointToGraph(int slot, int graph, Vector2 point){
		point.x = graphSlots[slot].Anchor.localPosition.x - point.x;
		point.y = graphSlots[slot].Anchor.localPosition.y + point.y;
		graphSlots[slot].graphs[graph].loadNode(point);
	}
	
}
