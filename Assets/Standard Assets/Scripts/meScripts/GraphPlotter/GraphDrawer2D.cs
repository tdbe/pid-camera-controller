﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class GraphDrawer2D{
	
	private QueueBuffer<Vector3> buff;
	private Material mat;
	
	
	public GraphDrawer2D(int n, Material mat){
		
		buff = new QueueBuffer<Vector3>(n);
		this.mat = mat;
	}
	
	public void loadNode(Vector2 node){
		
		buff.addItem(node.xy0());
	}
	
	
	public void doDrawPass(float scale){
		Vector3[] buffArr = buff.GetArray();
		Array.Reverse(buffArr);
		
		if(buffArr.Length < 2){
			return;
		}
		
		Vector3 first;
		Vector3 second;
		
		for (int i=1; i<buffArr.Length; i++){
			
			first = buffArr[i-1].xy0();
			second = buffArr[i].xy0();
			
			first.x -= (i-1)*scale;
			second.x -= i*scale;
			
			glDrawLine(first,
			           second);
		}
		
		
		
	}
	
	private void glDrawLine(Vector3 a, Vector3 b){
		GL.PushMatrix();
		mat.SetPass(0);//which pass of the shader
		GL.LoadOrtho();
		GL.Begin(GL.LINES);
		GL.Color(Color.red);
		//GL.Color(mat.color);
		
		GL.Vertex(a);
		GL.Vertex(b);
		
		GL.End();
		GL.PopMatrix();
	}
}
