﻿using UnityEngine;
using System.Collections;

public class PIDResolver{

	private StandardPIDGraphSet visPIDGraphs;

	private float errorSum = 0f;
	//private float prevError = 0f;
	private float prevInput;
	private float output = 0;
	

	private bool debugGraph = false;

	public PIDResolver(float initInput, float initSetpoint){

		//InvokeRepeating("testGraph", 0, 1);
		prevInput = initInput;
		//prevSetpoint = initSetpoint;
	}
	

	public void debugGraphSetActive(bool active){
		if(active && !debugGraph){
			visPIDGraphs = new StandardPIDGraphSet(Camera.main.GetComponent<GraphManager>());
			debugGraph = true;
		}
		else if(!active && debugGraph){
			visPIDGraphs.removeThisGraphSlot();
		}
	}
	

	public float PIDSolve(float setPoint, float input, PIDPropertiesIndividual settings, PIDPropertiesGlobal settsGlobal)
	{

		if(settings.isOn){
			//compute error vars

			//SetPoint - Input;
			float error = setPoint - input;

			errorSum = errorSum * settings.deweighting + settings.KI * error;// * Time.deltaTime;


			//float dErr = (error - prevError) / Time.deltaTime;
			//prevent derivative overshooting to infinity: http://brettbeauregard.com/blog/2011/04/improving-the-beginner%E2%80%99s-pid-derivative-kick/
			float dInput = input - prevInput;
			//TODO maybe the input needs a low pass filter, depending where you poll it from
			//Mathf.SmoothDamp(input.position.y, prevInput.y, ref velocity, 0.1f);

			//compute PID

			float tP = Utils.Clampf( settings.KP * error, settings.errSignalClamp);

			//float tI = KI * errorSum;
			float tI = Utils.Clampf( errorSum, settings.errSignalClamp);

			//float tD = KD * dErr;
			float tD = Utils.Clampf( settings.KD * dInput, settings.errSignalClamp);



			//prevError = error;
			prevInput = input;
			//prevSetpoint = setPoint;

			//output = (tP + tI + tD)* quickErrScale;
			output = (tP + tI - tD)* settings.quickErrScale;
			//Debug.Log(output);
			//input.position = new Vector3(input.position.x, input.position.y + output, input.position.z);

			//optional visualisation step
			if(debugGraph){
				visPIDGraphs.updatePIDGraphs(output, tP, tI, tD); 
			}

			return input + output;
		}
		else{
			return input;
		}
		
	}
	





}
