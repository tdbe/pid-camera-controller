﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// All these PID properties need to be polled due to changes received from the inspector in realtime.
/// </summary>
[System.Serializable]
public class PIDPropertiesIndividual{
	public bool isOn = true;
	public float KP = 0.12f;
	public float KI = 0.0125f;
	public float KD = -0.25f;
	[Range(0.000001f, 1.0f)]
	public float quickErrScale = 1f;
	[Range(0f, 1.0f)]
	public float deweighting = 0.9f;
	[Range(0.000001f, 50.0f)]
	public float errSignalClamp = 10f;
	//TODO: Note: a safer way to write this would be to
	//make all these values private and serializable, then to have
	//a public version with just a getter
	//That is, if you don't want to allow free alteration of the settings from code.
	//In my case, only the PIDProcessor and PIDResolver has access to these.
}
[System.Serializable]
public class PIDPropertiesGlobal
{
	[Range(0.0f, 2.0f)]
	public float updateInterval = 0.016f;
	[HideInInspector]
	public float lastUpdateTime = 0;
}

[System.Serializable]
public class PIDProcessorProperties{
	//good quick reference: http://www.michaeljohnstephens.com/devblog/organizing-the-unity-inspector-without-editor-scripts
	[Space(10)]
	public PIDPropertiesGlobal global;
	//The individual properties unique to each resolver instance.
	[Header ("PID Settings, this order: 0=X, 1=Y, 2=Z etc.")] 
	public PIDPropertiesIndividual[] individual;
	
	public void setAllControllersActive(bool val){
		for(int i = 0; i < individual.Length; i++){
			individual[i].isOn = val;
		}
	}
	public void setControllerActive(int index, bool val){
		if(index > -1 && index < individual.Length){
			individual[index].isOn = val;
		}
	}
}

/// <summary>
///	When created, each PIDProcessor is configured to work on a specific type of object (float, Vector2, Vector3, or Vector4).
///	This type of object is dictated by the type of the (setPoint, input) pair it receives on initializeProcessor(setPoint, input).
///	It also has a PIDProcessorProperties object (the serializable class above) which must contain user defined settings 
/// for each component of the object type (e.g. a Vector 3 needs 3 properties in the PIDProcessorProperties.Individual).
///	If the number of properties is valid, then it proceeds with creating a PIDResolver(float, float) instance for each component.
/// From here on, the current PIDProcessor instance provides the doUpdate(setPoint, input) function which uses the
///	PIDResolvers and returns the appropriate result if the request was valid.
/// </summary>
[System.Serializable]
public class PIDProcessor{
	[Space(10)]
	//-2 means "off", -1 means "everything overlayed", 0 to 3 are X, Y, Z and W on their own.
	//of course, if you set the flag to greater than the nr of params this processor actually has,
	//then the value defaults to the last param.
	//For now, it can only be set before starting the game. TODO: add an update check
	[Range(-2, 3)]
	public int axisToDebugInGraph = -1;
	
	[Space(10)]
	[SerializeField]
	private PIDProcessorProperties PIDProperties;

	private PIDResolver[] resolvers;

	//This int holds how many properties the input for this instance has.
	//So if on start you say you want this PIDProcessor to work on Vector3's, then
	//type.val will become 3, and this processor will no longer accept input or settings 
	//which do not match a Vector3.
	private struct MyReadonlyInt{
		//I'm making my own readonly int because I don't have a constructor in this serialized class, so regular readonly doesn't work.
		public readonly int val;
		public MyReadonlyInt(int val){
			this.val = val;
		}
	}
	private MyReadonlyInt type;
	

	public void initializeProcessor(float setPoint, float input) {
		init(1, new Vector4(setPoint, 0, 0, 0), new Vector4(input, 0, 0, 0));
	}
	public void initializeProcessor(Vector2 setPoint, Vector2 input) {
		init(2, (Vector4)setPoint, (Vector4)input);
	}
	public void initializeProcessor(Vector3 setPoint, Vector3 input) {
		init(3, (Vector4)setPoint, (Vector4)input);
	}
	public void initializeProcessor(Vector4 setPoint, Vector4 input) {
		init(4, setPoint, input);
	}

	private void init(int size, Vector4 setPoint, Vector4 input){
		type = new MyReadonlyInt(size);

		if(PIDProperties.individual.Length != size){
			Debug.LogError("This function expects a number of "+size+
			               " Individual PID Properties, but it received "+
			               PIDProperties.individual.Length+
			               ". Are you sure you configured the Properties correctly?");
		}

		resolvers = new PIDResolver[size];
		for(int i = 0; i < size; i++){
			resolvers[i] = new PIDResolver(setPoint[i], input[i]);
		}


		if(axisToDebugInGraph>-1){
			if(axisToDebugInGraph < size)
				resolvers[axisToDebugInGraph].debugGraphSetActive(true);
			else
				resolvers[size-1].debugGraphSetActive(true);
		}
		else if(axisToDebugInGraph == -1){
			for(int i=0; i<size; i++){
				resolvers[i].debugGraphSetActive(true);
			}
		}
	}


	public float doUpdate(float setPoint, float input){
		if(validate(1)){
			return handleUpdate(new Vector4(setPoint, 0, 0, 0), new Vector4(input, 0, 0, 0)).x;
		}
		else
			return input;
	}
	public Vector2 doUpdate(Vector2 setPoint, Vector2 input){
		if(validate(2)){
			return (Vector2)handleUpdate((Vector4)setPoint, (Vector4)input);
		}
		else
			return input;
	}
	public Vector3 doUpdate(Vector3 setPoint, Vector3 input){
		if(validate(3)){
			return (Vector3)handleUpdate((Vector4)setPoint, (Vector4)input);
		}
		else
			return input;
	}
	public Vector4 doUpdate(Vector4 setPoint, Vector4 input){
		if(validate(4)){
			return handleUpdate(setPoint, input);
		}
		else
			return input;
	}

	private bool validate(int requestedSize){
		if(requestedSize != type.val){
			Debug.LogError("The type you are sending to this PIDProcessor"+
			               " does not correspond to the number of args ("+type.val+
			               ") of the type you initialized the processor with!");
			return false;
		}

		//We only run the PID Resolvers as fast as the settings say we should.
		//an updateInterval of 0.0f will essentially be realtime. default is 0.016 == 60fps.
		//From a practical standpoint though, this feature is not really needed in here, 
		//since this will be called from a FixedUpdate(); anything slower or faster isn't useful.
		//Classic PID controllers have variable operating frequencies though... 
		if(Time.time - PIDProperties.global.lastUpdateTime >= PIDProperties.global.updateInterval){
			PIDProperties.global.lastUpdateTime = Time.time;
			return true;
		}
		else
			return false;
	}

	//I'm casting everything to vector4 so I don't have to copy myself 4 times in code, and 
	//so I can use the [index] operator.
	//But the for loop only has 2 iterations if the original type was Vector2, for example.
	private Vector4 handleUpdate(Vector4 setPoint, Vector4 input){
		Vector4 newPosition = input;
		for(int i=0; i<type.val; i++){
			newPosition[i] = 
				resolvers[i].PIDSolve(setPoint[i],
	                                    input[i],
	                                   	PIDProperties.individual[i],
	                                    PIDProperties.global
	                                    );
		}
		return newPosition;
	}


}
