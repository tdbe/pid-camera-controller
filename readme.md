I wrote a Proportional-Integral-Derivative control system, and graph visualisation. 

Used in this case, to control a third person camera's XYZ position and sometimes Rotation.

WIP

=====================

Demo WebPlayer build:
https://itu.dk/people/tdbe/pidtest/

=====================

Controls:

	Keyboard:
		WASD to move, Shift to walk
		Hold Space to become slowmo while in the air
		Ctrl to crouch
		Mouse to rotate camera
		//CapsLock to lock camera onto target

	Controller settings:
	 	L analog move
		R analog move camera
	 	X to jump and to use slowmo mode
	 	O to crouch
	 	//RB to lock onto target
=====================

Graph legend:

	 	White: base x axis.
	 	Blue: Proportional component
	 	Red: Integral component
	 	Green: Derivative component
	 	Black: P+I+D output

Note: in the current build the visualization is set to output 3 overlayed graphs, one for each coordinate (x, y, z in worldspace). So you'll potentially see 3x each graph type, depending on your direction of movement.